package com.test.vampires.fragments;

import java.util.HashMap;

import android.R;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.test.vampires.activities.MainActivity;
import com.test.vampires.data.Human;
import com.test.vampires.data.Vampire;
//import com.google.android.gms.location.LocationListener;


public class MapFragment extends SupportMapFragment {

	public static GoogleMap map;
	private static Circle circle;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
	}

	
	@Override
	public void onResume() {
		setupMap();
		super.onResume();	
	}
	

	private void setupMap() {
		
		if (map == null){ 
			map = getMap(); 
			if (map != null){ 
				map.moveCamera(CameraUpdateFactory.newLatLngZoom(Vampire.currentLatLng, 17)); // <-- Mueve la camera a las coordenadas que le he definido con la constante, y el nivel de zoom
				map.getUiSettings().setZoomControlsEnabled(false); // Quito los botones de zoom (solo funciona con los gestos)
				map.setMyLocationEnabled(true); // Con esto aparece el icono arrriba a la derecha que cuando se clica ubica en el mapa con un puntito azul
			}
		}
				
	}
	
	
	@Override
	public void onPause() {
		
		super.onPause();
		map = null;
		
	}


	// Redibuja los humanos contenidos en humansList
	public static void drawHumans(HashMap <Integer, Human> humansList){
		
		map.clear(); // <-- Limpiamos el mapa de los marcadores antiguos
		
		for (Human human : humansList.values()){ // <-- Recorro todos los elementos del HasMap de humanos
			MarkerOptions options = new MarkerOptions().position(human.getPosition())
				   .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_notification_overlay))
				   .title("Human: " + String.valueOf(human.getId()))
				   .snippet("speed="+String.valueOf(human.getSpeed()) + ", points =" + String.valueOf(human.getPoints()));
		
			map.addMarker(options);
		}
	}

	
	// Dibuja un circulo en la posici�n indicada por latlng del color que se le indique
	public static void redrawCircle(LatLng latlng, int circleColor) {	
	
		if (circle != null) circle.remove();
		
		CircleOptions circleOptions = new CircleOptions().center(latlng) // Centrado en la posici�n que se nos pasa
												         .radius(Vampire.ACTION_RADIO)   // Radio definido en main
												         .fillColor(Color.TRANSPARENT)
												         .strokeColor(circleColor)
												         .strokeWidth(2);

        circle = map.addCircle(circleOptions);		
	}
	
	public static void refreshMap(){	
		
		int color = Color.BLACK;
		if (Vampire.humanDetected) color= Color.RED; 
		
		drawHumans(MainActivity.humansList);
		redrawCircle(Vampire.currentLatLng, color);
		/*
		if (Vampire.humanDetected = true){
			// Habilitamos la opci�n de sacar los colmillos
			Log.i("Mapa", "Human " + Vampire.nearestHuman.getId() + " targeted! (" + Vampire.nearestHuman.getPoints() + ")");
			//StatusFragment.teethImageView.setVisibility(View.VISIBLE);
			//StatusFragment.teethImageView.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.blink));
				
		}else {
		//StatusFragment.teethImageView.setVisibility(View.INVISIBLE);
		}
		*/
		
	}

}
