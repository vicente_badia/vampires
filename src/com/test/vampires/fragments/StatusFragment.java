package com.test.vampires.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.test.vampires.R;
import com.test.vampires.activities.MainActivity;
import com.test.vampires.data.Vampire;

public class StatusFragment extends Fragment{
		
	// UI	
	public static ImageView teethImageView;
	public static ProgressBar teethProgressBar;

	public static TextView pointsTextView;
	public static TextView countDownTextView;
	

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_status, container, false);
        
        teethImageView = (ImageView) view.findViewById(R.id.imageViewTeeth);
		teethProgressBar = (ProgressBar) view.findViewById(R.id.progressBarTeeth);
		teethProgressBar.setMax((int) Vampire.TEETH_TIME);
		teethProgressBar.setProgress((int) Vampire.TEETH_TIME);

        pointsTextView = (TextView) view.findViewById(R.id.textViewPoints);
        countDownTextView = (TextView) view.findViewById(R.id.textViewCountDown);      
        
        // Actualizar con los valores actuales
        pointsTextView.setText(String.valueOf(Vampire.points));
        
        // Listener para el bot�n de sacar los dientes
        teethImageView.setOnClickListener(new OnClickListener() {
			
		@Override
		public void onClick(View v) {
				
			Vampire.teethOut = true;
			teethImageView.setEnabled(false); // <-- Mientras los dientes est�n fuera no se puede clicar de nuevo hasta que se acabe el tiempo
				
			if (Vampire.nearestHuman != null){
									Vampire.points = Vampire.points + Vampire.nearestHuman.getPoints();
				MainActivity.humansList.remove(Vampire.nearestHuman.getId());
				Vampire.updateNearestHuman(MainActivity.humansList); // <-- Se vuelve a actualizar el humano m�s pr�ximo
				MapFragment.refreshMap();
				refreshStatus();
			}

			new CountDownTimer(Vampire.TEETH_TIME, 100) {  // <-- SOLUCION PROVISIONAL PARA PRUEBAS. EN VERSI�N DEFINITIVA HAY QUE BUSCAR ALGO M�S COMPLEJO

				public void onTick(long millisUntilFinished) {
					countDownTextView.setText(String.valueOf(millisUntilFinished / 1000));
					teethProgressBar.setProgress((int) millisUntilFinished);
				}

				public void onFinish() {
					 Vampire.teethOut = false;
					countDownTextView.setText(String.valueOf(Vampire.TEETH_TIME/1000));
					teethProgressBar.setProgress((int) Vampire.TEETH_TIME);
					teethImageView.setEnabled(true); // <-- Se pueden volver a sacar los colmillos		 
					refreshStatus();
				}
			}.start();
			
			teethImageView.clearAnimation(); // <-- Los colmillos ya no parpadean

			}
		});       
        
        return view;
        
   }
	

	@Override
	public void onResume() {
		super.onResume();
	}


	public void refreshStatus() {
		
		pointsTextView.setText(String.valueOf(Vampire.points));
		
		if (Vampire.humanDetected) {
			teethImageView.setVisibility(View.VISIBLE); 
			teethImageView.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.blink)); // <-- Y vuelven a parpadear
			Log.i("Vampire", "Teeth VISIBLE");
		} else {
			teethImageView.setVisibility(View.INVISIBLE);
			teethImageView.clearAnimation();
			Log.i("Vampire", "Teeth INVISIBLE");
			//teethImageView.setEnabled(false); // <-- No se pueden sacar los colmillos
		}
	}
	
	

	
}
