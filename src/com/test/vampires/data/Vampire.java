package com.test.vampires.data;

import java.util.HashMap;

import android.util.Log;
import com.google.android.gms.maps.model.LatLng;

// Clase que representa al propio jugador (vampiro)
public class Vampire {
	
	//public static final LatLng TRES_CANTOS = new LatLng(40.60, -3.71); // De momento experimento en tres cantos city
	//public static LatLng currentLatLng = TRES_CANTOS; // Por defecto me pongo aqu� por si no pudiera recibir la ubiaci�n
	
	public static final double ACTION_RADIO = 25; // <-- Radio de acci�n del vampiro
	public static final long TEETH_TIME = 5000; // <-- Tiempo que los colmillos pueden estar sacados
	
	public static int points = 0;
	
	public static Boolean teethOut = false;	
	public static Boolean humanDetected = false;
	
	public static Human nearestHuman;
	public static double distanceToNearestHuman = 10000; /// EVITAR HARD-CODED 
	public static LatLng currentLatLng; 

	
	public Vampire(LatLng currentLatLng, int points){
		
		Vampire.currentLatLng = currentLatLng;
		Vampire.points = points;
		
	}
	
	
	
	// Actualiza el humano m�s pr�ximo
	public static void updateNearestHuman(HashMap<Integer, Human> humansList){
			
		double distance;
		Vampire.distanceToNearestHuman = 10000;
		
	    if (humansList.isEmpty()){
	    	Vampire.nearestHuman = null;
	    	Vampire.humanDetected = false;
	    	return;
	    }
				
		for (Human human : humansList.values()){
			
			distance = human.distanceTo(Vampire.currentLatLng);  
			Log.d("Distancias", "Distancia al humano " + human.getId() + " = " + distance);
			if (distance < Vampire.distanceToNearestHuman){
				Vampire.distanceToNearestHuman = distance;
				Vampire.nearestHuman = human;
			}			
		}
			 
		Log.d("Distancias", "Distancia minima = " + Vampire.distanceToNearestHuman + "(Humano " + Vampire.nearestHuman.getId() +")");
			
		// Comprobamos si hay alg�n humano dentro de el radio de acci�n
		if (distanceToNearestHuman< ACTION_RADIO){
			Vampire.humanDetected = true;
			Log.i("Vampire", "Human detected = TRUE");
															
		} else {
			Vampire.humanDetected = false;
			Log.i("Vampire", "Human detected = FALSE");
							
		}			
	}	
}


