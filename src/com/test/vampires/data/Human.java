package com.test.vampires.data;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

// Clase que representa cada humano que el vampiro puede cazar
public class Human {

	 private static int numberOfHumans = 0; // <-- Confirmar si ser� necesario
	 
	 private int id;
	 private int points;
	 private int speed; 
	 // A�ADIR SPEED_X Y SPPED_Y
	 private LatLng position;
	// private String name; // <-- En un futuro se le puede asignar un nombre al azar a cada humano
	
	 
	public Human(int points, int speed, LatLng position){
		
		this.id = numberOfHumans;
		this.points = points;
		this.speed = speed;
		this.position = position;
		
		numberOfHumans++ ;
		
	}
	
	public double distanceTo(LatLng otherLatlng){
		
		double latitudeHuman = this.position.latitude;
		double longitudeHuman = this.position.longitude;
		double latitudeOther = otherLatlng.latitude;
		double longitudeOther = otherLatlng.longitude;
		float results[] = {0};
		
		Location.distanceBetween(latitudeHuman, longitudeHuman, latitudeOther, longitudeOther, results); // SUSTITUIR POR DISTANCE_TO
		
		return (double)results[0];
		
	}
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public LatLng getPosition() {
		return position;
	}
	public void setPosition(LatLng position) {
		this.position = position;
	}

	
	 
}

