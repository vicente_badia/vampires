package com.test.vampires.activities;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;
import com.test.vampires.R;
import com.test.vampires.data.Human;
import com.test.vampires.data.Vampire;
import com.test.vampires.fragments.MapFragment;
import com.test.vampires.fragments.StatusFragment;



/* Actividad principal del juego (confirmar con David el nombre)
 * Consta de dos fragmentos (el mapa donde se juega) y una status bar en la parte superior para 
 * realizar las acciones de "sacar los colmillos" y tener info sobre puntuaci�n y tiempo
 */

public class MainActivity extends FragmentActivity
						  implements OnConnectionFailedListener,
						  			 LocationListener{ // <-- Listener para avisar que en el mapa hay un humano dentro del radio de acci�n
	
	private static final int NUMBER_OF_HUMANS = 5;
	    
    public static final long UPDATE_INTERVAL_IN_MILISECONDS = 2000; // <-- 2 segundos
    public static final long FAST_INTERVAL_CEILING_IN_MILISECONDS = 1000; // <-- 1 segundo
     
    public final static int CONNECTION_FAILURE_REQUEST = 9000;
	
	public static HashMap<Integer, Human> humansList = null ; // TBC
	
	// Location
	LocationManager locationManager;
	Location location;
	List<String> listaProviders;

	
	//Fragments
	Fragment mapFragment = new MapFragment();
	Fragment statusFragment = new StatusFragment();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		setUpConnection();
		
		if (humansList==null){
			generateHumans();
		}
		
	}

	
	@Override
	protected void onResume() {
		
		super.onResume();
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		fragmentTransaction.add(R.id.status_frame, statusFragment);
		fragmentTransaction.add(R.id.map_frame, mapFragment);
		fragmentTransaction.commit();
		
		if (servicesConnected()){ // Comprobaci�n de que GooglePlayServices est� disponible

			if (MapFragment.map != null)  {
				MapFragment.refreshMap();
			}
		} else {
				Toast.makeText(this, "Map couldn't be loaded", Toast.LENGTH_SHORT).show();
				// Ocultar fragment si fuera necesario
		}
	}
	

	@Override
	protected void onPause() {
		
		super.onPause();
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		fragmentTransaction.remove(mapFragment);
		fragmentTransaction.remove(statusFragment);
		fragmentTransaction.commit();
		
	}


	// Comprobaci�n de que GooglePlayServices est� disponible
	private boolean servicesConnected() {
		
		int code =	GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
				
		if (code == ConnectionResult.SUCCESS){
			return true;
			} else { // Ha habido un fallo, as� que tengo que avisar y devolver false
					Dialog dialog = GooglePlayServicesUtil.getErrorDialog(code, this, 0);
					if (dialog != null){ 
						dialog.show();					
					}
					return false;	
			}				
	}


	// Callback que avisa si la conexi�n a GoogleMaps falla o se pierde
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		
		if (connectionResult.hasResolution()){
			try{
				connectionResult.startResolutionForResult(this,CONNECTION_FAILURE_REQUEST);
			}catch(IntentSender.SendIntentException e){
				Log.e("ERROR",Log.getStackTraceString(e));
			}	
		} else { 
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, CONNECTION_FAILURE_REQUEST);
			if (dialog != null){ // Si el dialogo es distinto de null, lo muestro
				dialog.show();	
			}		
		}		
	}
	
	// Rellena el HashMap humansList con humanos creado al azar en torno a la posici�n actual
	private void generateHumans (){
		
		humansList = new HashMap <Integer, Human>();
		
		LatLng position;
			
		for (int i = 0 ; i< NUMBER_OF_HUMANS; i++){
			
			Random random = new Random();
				
			double x = (random.nextFloat()-0.5)*0.001 + Vampire.currentLatLng.latitude; // Distancia random de -0.005 a +0.005
			double y = (random.nextFloat()-0.5)*0.001 + Vampire.currentLatLng.longitude; // Distancia random de -0.005 a +0.005
							
			position = new LatLng(x,y);

			int speed = random.nextInt(10); // <-- Velocidad del humano generado
			int points = 200*(speed); // <-- Puntuaci�n es 200 veces la velocidad 
				
			Log.i("Generacion humanos", i + " --> " + speed + " , " + points);
				
			Human human = new Human(points, speed, position);
			humansList.put(human.getId(), human);
			
		}
		
		Vampire.updateNearestHuman(humansList);
	}
	
	
	
	void setUpConnection () {
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		listaProviders = locationManager.getAllProviders();
		 
		//LocationProvider provider = locationManager.getProvider(listaProviders.get(0));
		
		// Toast.makeText(getActivity(), "Proveedor:  " + listaProviders.get(0), Toast.LENGTH_LONG).show();
		
		
		locationManager.requestLocationUpdates(listaProviders.get(0), UPDATE_INTERVAL_IN_MILISECONDS, 0, this); // En listraProvider[0] tengo el mejor proveedor disponible	
		location = locationManager.getLastKnownLocation(listaProviders.get(0));
		
		
		if (location!=null){ 
			
			double x = location.getLatitude();
			double y = location.getLongitude();
	
			Vampire.currentLatLng = new LatLng(x, y);

			Toast.makeText(this, "Posici�n inicial: x = " + x + " , y = " + y, Toast.LENGTH_SHORT).show();		
	}
}
		
		
		
	// Callback para cuando nos movemos
	public void onLocationChanged(Location newLocation) {
	    		    	
	    float x = (float) newLocation.getLatitude();
		float y = (float) newLocation.getLongitude();
			
		Vampire.currentLatLng = new LatLng(x, y);
	    
		Vampire.updateNearestHuman(humansList);
		
		MapFragment.refreshMap();
		((StatusFragment) statusFragment).refreshStatus(); // <<<<<<---- ESTO ESTA MAL. NO ESTA HACIENDO NADA!!!!
			
	    //Toast.makeText(this, "Nueva posici�n: x = " + x + " , y = " + y, Toast.LENGTH_SHORT).show();
	 
	   }	
	
	@Override
	public void onProviderDisabled(String arg0) {
		Toast.makeText(this, "Proveedor OFF", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Proveedor ON", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Cambio de estado en el proveedor", Toast.LENGTH_LONG).show();
	}

	

}
