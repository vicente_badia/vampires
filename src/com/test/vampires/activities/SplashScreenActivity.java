package com.test.vampires.activities;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;

import com.test.vampires.R;


/* Actividad donde se muestra el splash inicial y se comprueba si es necesario ir a la pantalla de log-in
 * o por el contrario ya est� "logueado" y se puede ir directamente a la actividad principal
 */



public class SplashScreenActivity extends Activity {

	// Duraci�n del Splash
    private static final long SPLASH_SCREEN_DELAY = 1000; 

    //private  Bundle applicationStarted = null; // <-- Para evitar que se muestre el splash si ya estamos jugando
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	

    	super.onCreate(savedInstanceState);
    	    	
        // Forzamos orientaci�n vertical y ocultamos la barra de estado de Android
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
 
        setContentView(R.layout.splash_screen);
 
        // Hacemos la espera de forma as�ncrona
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Siguiente actividad
                Intent mainIntent = new Intent().setClass(SplashScreenActivity.this, MainActivity.class);
                startActivity(mainIntent);
 
                // Cerramos esta actividad para que el usuario no pueda volver al splash al pulsar back
                finish();
            }
        };
 
        // Simulamos la carga de datos para esta versi�n beta
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
 
}
